﻿using System.Text;

namespace Pyramid
{
    /// <summary>
    /// Helps to build a pyramid
    /// </summary>
    public class PyramidBuilder
    {
        /// <summary>
        /// Returns string representation of a pyramid.
        /// </summary>
        /// <param name="height">height of a Pyramid. For values less or equal to zero returns empty string</param>
        /// <param name="freeSpaceChar">symbol to use as a free space</param>
        /// <param name="buildBlockChar">symbol to use as a pyramid build block</param>
        /// <returns></returns>
        public static string Build(
            int height,
            char freeSpaceChar = ' ',
            char buildBlockChar = '*')
        {
            var pyramid = new StringBuilder();

            var buildBlocksLen = 1;
            var freeSpacesLen = height - 1;

            while (freeSpacesLen >= 0)
            {
                pyramid.Append(new string(freeSpaceChar, freeSpacesLen));
                pyramid.AppendLine(new string(buildBlockChar, buildBlocksLen));

                buildBlocksLen += 2;
                freeSpacesLen -= 1;
            }

            return pyramid.ToString();
        }
    }
}