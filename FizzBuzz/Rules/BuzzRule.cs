﻿namespace FizzBuzz
{
    public class BuzzRule : IRule
    {
        public bool CanApply(int val, string input) => val % 5 == 0;
        public string Apply(int val, string input) => input + "Buzz";
    }
}
