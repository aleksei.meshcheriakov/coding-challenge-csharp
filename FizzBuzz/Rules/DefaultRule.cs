﻿namespace FizzBuzz
{
    public class DefaultRule : IRule
    {
        public bool CanApply(int val, string input) => input == string.Empty;

        public string Apply(int val, string input) => val.ToString();
    }
}
