﻿namespace FizzBuzz
{
    public class FooRule : IRule
    {
        public bool CanApply(int val, string input) => val * 10 > 100;
        public string Apply(int val, string input) => input + "Foo";
    }
}
