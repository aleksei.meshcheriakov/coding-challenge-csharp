﻿namespace FizzBuzz
{
    public class FizzRule : IRule
    {
        public bool CanApply(int val, string input) => val % 3 == 0;
        public string Apply(int val, string input) => input + "Fizz";
    }
}
