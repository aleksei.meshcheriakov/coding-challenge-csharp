﻿namespace FizzBuzz
{
    /// <summary>
    /// Keeps the collection of rules instances for quick access to the rules.
    /// </summary>
    public static class Rules
    {
        /// <summary>
        /// Applicable if val is dividable by 3
        /// </summary>
        public static readonly IRule Fizz = new FizzRule();

        /// <summary>
        /// Applicable if val is dividable by 5
        /// </summary>
        public static readonly IRule Buzz = new BuzzRule();

        /// <summary>
        /// Applicable if val is dividable by 7
        /// </summary>
        public static readonly IRule Bar = new BarRule();

        /// <summary>
        /// Applicable if val * 100 > 100
        /// </summary>
        public static readonly IRule Foo = new FooRule();

        /// <summary>
        /// Returns val string representation in case of empty input, otherwise returns input itself
        /// </summary>
        public static readonly IRule Default = new DefaultRule();
    }
}
