﻿namespace FizzBuzz
{
    public class BarRule : IRule
    {
        public bool CanApply(int val, string input) => val % 7 == 0;
        public string Apply(int val, string input) => input + "Bar";
    }
}
