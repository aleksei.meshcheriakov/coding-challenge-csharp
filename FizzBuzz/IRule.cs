﻿namespace FizzBuzz
{
    public interface IRule
    {
        /// <summary>
        /// Check can rule be applied to the val
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        bool CanApply(int val, string input);

        /// <summary>
        /// Returns string output based on val and input parameters
        /// </summary>
        /// <param name="val"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        string Apply(int val, string input);
    }
}
