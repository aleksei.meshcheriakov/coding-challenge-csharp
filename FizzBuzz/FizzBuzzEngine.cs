﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz
{
    /// <summary>
    /// Engine contains queue of rules, each of them can be marked as final.
    /// During each value processing, engine is going through the rules from first to last and tried to apply
    /// to the value. If applicable then rule is applied and the output from the rule is going as input to the next rule and so on.
    /// If some rule was applicable and it was marked as final, then engine stops processing on this rule (after this rule was applied)
    /// </summary>
    public class FizzBuzzEngine
    {
        private readonly List<(IRule rule, bool final)> _ruleQueue;
        private readonly Action<string> _output;

        /// <summary>
        /// Constructor with default output to console.
        /// </summary>
        /// <param name="ruleQueue">queue of rules. If some rule marked as final then processing will be stopped on it if the rule was applicable</param>
        public FizzBuzzEngine(IEnumerable<(IRule rule, bool final)> ruleQueue)
            :this(ruleQueue, line => Console.WriteLine(line))
        {
        }

        /// <summary>
        /// Constructor which can accept action to output to any target (would be useful for tests)
        /// </summary>
        /// <param name="ruleQueue">queue of rules. If some rule marked as final then processing will be stopped on it if the rule was applicable</param>
        /// <param name="output">every processed value will be sent to this output</param>
        public FizzBuzzEngine(
            IEnumerable<(IRule rule, bool final)> ruleQueue,
            Action<string> output)
        {
            _ruleQueue = ruleQueue.ToList();
            _output = output;
        }

        public void Run(int limit = 100)
        {
            for (int i = 1; i <= limit; i++)
            {
                _output($"{i}: {ApplyRuleQueueToValue(i)}");
            }
        }

        /// <summary>
        /// apply the whole rule queue to the specified value
        /// </summary>
        /// <param name="val">value to which the whole rule queue will be applied</param>
        /// <returns></returns>
        public string ApplyRuleQueueToValue(int val)
        {
            var output = "";

            foreach (var rule in _ruleQueue)
            {
                if (rule.rule.CanApply(val, output))
                {
                    output = rule.rule.Apply(val, output);
                    if (rule.final)
                    {
                        break;
                    }
                }
            }

            return output;
        }
    }
}
