﻿using System;
using System.Collections.Generic;

namespace FizzBuzz
{
    public class FizzBuzzEngineBuilder
    {
        private readonly List<(IRule rule, bool final)> _rulesQueue = new List<(IRule rule, bool final)>();
        private Action<string> _output = line => Console.WriteLine(line);

        /// <summary>
        /// Overrides the default output to console
        /// </summary>
        /// <param name="output"></param>
        /// <returns></returns>
        public FizzBuzzEngineBuilder SetOutput(Action<string> output)
        {
            _output = output;
            return this;
        }

        /// <summary>
        /// Adds rule to the rules processing queue. If rule specified
        /// as final then the engine stops the rules processing after the first applied rule
        /// </summary>
        /// <param name="rule">rule to add</param>
        /// <param name="final">If true then the engine stops the rules processing after the first applied rule</param>
        /// <returns></returns>
        public FizzBuzzEngineBuilder AddRule(IRule rule, bool final = false)
        {
            _rulesQueue.Add((rule, final));
            return this;
        }

        /// <summary>
        /// Build the engine
        /// </summary>
        /// <returns></returns>
        public FizzBuzzEngine Build()
        {
            return new FizzBuzzEngine(_rulesQueue, _output);
        }
    }
}
