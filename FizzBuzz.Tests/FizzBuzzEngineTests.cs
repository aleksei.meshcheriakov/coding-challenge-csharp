using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace FizzBuzz.Tests
{
    [TestClass]
    public class FizzBuzzEngineTests
    {
        [TestMethod]
        public void Run_CheckTheStandardConfiguation_TheOutputWithLimit16()
        {
            // Arrange
            var output = new List<string>();

            var engine = new FizzBuzzEngineBuilder()
                .SetOutput(output.Add)
                .AddRule(Rules.Fizz)
                .AddRule(Rules.Buzz)
                .AddRule(Rules.Default)
                .Build();

            // Act
            engine.Run(16);

            // Assert
            CollectionAssert.AreEquivalent(
                new[] {
                    "1: 1",
                    "2: 2",
                    "3: Fizz",
                    "4: 4",
                    "5: Buzz",
                    "6: Fizz",
                    "7: 7",
                    "8: 8",
                    "9: Fizz",
                    "10: Buzz",
                    "11: 11",
                    "12: Fizz",
                    "13: 13",
                    "14: 14",
                    "15: FizzBuzz",
                    "16: 16"},
                output);
        }

        [DataTestMethod]
        [DataRow(true, "Fizz")]
        [DataRow(false, "FizzBuzz")]
        public void Run_WhenFizzRuleIsFinal_Then15ShouldBeJustFizz(bool final, string expected)
        {
            // Arrange
            var output = new List<string>();

            var engine = new FizzBuzzEngineBuilder()
                .AddRule(Rules.Fizz, final)
                .AddRule(Rules.Buzz, final)
                .AddRule(Rules.Default)
                .Build();

            // Act, Assert
            Assert.AreEqual(expected, engine.ApplyRuleQueueToValue(15));
        }
    }
}
