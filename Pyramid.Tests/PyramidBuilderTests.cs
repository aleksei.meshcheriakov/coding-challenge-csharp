using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Pyramid.Tests
{
    [TestClass]
    public class PyramidBuilderTests
    {
        private readonly static string NL = Environment.NewLine;

        public static IEnumerable<object[]> TestCases => new List<object[]>
        {
            new object[] { -1, string.Empty },

            new object[] { 0, string.Empty },

            new object[] { 1, $"*{NL}" },

            new object[] { 3, $"  *{NL}" +
                              $" ***{NL}" +
                              $"*****{NL}" },

            new object[] { 5, $"    *{NL}" +
                              $"   ***{NL}" +
                              $"  *****{NL}" +
                              $" *******{NL}" +
                              $"*********{NL}" }
        };

        [DataTestMethod]
        [DynamicData(nameof(TestCases), DynamicDataSourceType.Property)]
        public void TestMethod1(int height, string expectedOutput)
        {
            // Arrange, Act
            var output = PyramidBuilder.Build(height);

            // Assert
            Assert.AreEqual(expectedOutput, output,
                $"For height {height} expected is{NL}" +
                $"{expectedOutput}{NL}" +
                $"but output was{NL}{output}");
        }
    }
}
